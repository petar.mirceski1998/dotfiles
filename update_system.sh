#!/usr/bin/env bash
set -e

update_apt() {
	sudo apt update
	sudo apt upgrade -y
}

update_nix() {
	nix upgrade-nix
	nix-channel --update
	nix-env -u --always
	home-manager switch
}

clean_nix() {
	# Clean nix
	# nix-env --delete-generations +5
	nix-store --gc --print-roots
	rm /nix/var/nix/gcroots/auto/* || true
	# nix-collect-garbage --delete-older-than 5d
	nix-collect-garbage -d # Full upgrade and forget
}

update_python() {
	# Update conda
	. ./setup/init_conda.sh
	conda update -y -n base -c defaults conda
	conda update -y -n base -c defaults --all
	conda activate dev
	conda env update --name dev --file setup/conda_environment.yml --prune
	conda update -y -n dev --all
	conda clean -y -a
	conda deactivate

	# Update pipx env
	bash ./setup/setup_python_utils.sh
}

update_node() {
	# Update npm tools
	npm install -g npm
	npm update -g
	bash ./setup/setup_eslint_server.sh

	# Update yarn env
	bash ./setup/setup_node_utils.sh

}

update_golang() {
	# GO111MODULE=off go get -u all
	bash ./setup/setup_golang_utils.sh

}

do_hacks() {
	# This file sometimes causes issues, clean it up periodically
	rm ~/.emacs.d/.local/etc/lsp-session || true

	# This should be done after home-manager switch
	# Copying full directory causes some permission errors when updating
	# due to locally generated flycheck elc files.
	sudo find "$HOME/.doom.d/" -type f -name '*.elc' -delete

	# Fish stores some universal variables here
	# So clean it up periodically to make it more consistent
	# See: https://github.com/fish-shell/fish-shell/issues/4542
	rm "$HOME/.config/fish/fish_variables" || true

	# Delete any broken symlinks in local bin
	find "$HOME/.local/bin" -xtype l -delete
}

update_doom_emacs() {
	"$HOME/.emacs.d/bin/doom" upgrade -f
	"$HOME/.emacs.d/bin/doom" purge -g
}

update_apt
update_nix
clean_nix
do_hacks
