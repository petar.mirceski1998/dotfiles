function clean
    nix-store --gc --print-roots
    rm /nix/var/nix/gcroots/auto/*
    # nix-collect-garbage --delete-older-than 5d
    nix-collect-garbage -d # Full upgrade and forget
end
