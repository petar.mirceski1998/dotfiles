function upgrade
    nix-channel --update
    and nix-env -u --always
    and home-manager switch
end
