;;; Code:
(setq user-full-name "Petar Mircheski"
      user-mail-address "petar.mirceski1998@gmail.com"
      display-line-numbers-type 'relative
      doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 19)
      doom-unicode-font (font-spec :family "JetBrains Mono")
      doom-font (font-spec :family "Inconsolata" :size 16)
      doom-big-font (font-spec :family "JetBrains Mono" :size 22)
      doom-theme 'doom-vibrant)

;; Set the splash image upon first loading of DOOM
(defun random-choice(choices)
  (elt choices (random (length choices))))
(defvar splash-art (directory-files (concat doom-private-dir "/splash_art") t "^\\([^.]\\|\\.[^.]\\|\\.\\..\\)"))
(defvar random-splash-image (random-choice splash-art))
(setq fancy-splash-image  random-splash-image)

;; Don't cut when pasting over selected text
(setq evil-kill-on-visual-paste nil)
;; SubWord mode for camelCase
(global-subword-mode t)
;; Don't spell check comments
(setq ispell-check-comments nil)
;; Transparency
(defconst doom-frame-transparency 95)

(defun not-transparent-frame ()
  "MAKE THE FRAME NOT TRANSPARENT."
  (interactive)
  (set-frame-parameter
    (selected-frame)
    'alpha (if (frame-parameter (selected-frame) 'fullscreen)
              100
             doom-frame-transparency)))

(defun transparent-frame()
  "MAKE THE FRAME TRANSPARENT."
  (interactive)
  (set-frame-parameter (selected-frame) 'alpha doom-frame-transparency)
  (add-to-list 'default-frame-alist `(alpha . ,doom-frame-transparency)))

(provide 'customize)
;;; customize.el ends here
