;;; jedi-language-server.el --- description -*- lexical-binding: t; -*-
;;; Commentary:
;;
;;  Integrate jedi-language-server with lsp-mode
;;
;;; Code:

(require 'lsp-mode)

(defgroup lsp-jedi nil
  "Jedi lsp."
  :group 'lsp-mode
  :tag "Jedi")

(defcustom lsp-jedi-command '("jedi-language-server")
  "Jedi lsp command."
  :group 'lsp-jedi
  :risky t
  :type 'list)

(lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection (lambda () lsp-jedi-command))
                  :major-modes '(python-mode)
                  :priority 0
                  :server-id 'jedi
                  ;; ignore some unsupported messages
                  :initialized-fn (lambda (workspace)
                                    (let ((caps (lsp--workspace-server-capabilities workspace)))
                                      (ht-set! caps "textDocumentSync"
                                                (ht ("save" 'nil)
                                                    ("willSave" 'nil)
                                                    ("change" 2)
                                                    )
                                               )
                                      )
                                    )
                  ))

(provide 'jedi-language-server)
;;; jedi-language-server.el ends here
