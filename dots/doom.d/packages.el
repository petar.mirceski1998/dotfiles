;; (package! prettier)
(package! gitconfig-mode
	  	  :recipe (:host github :repo "magit/git-modes"
				 			 :files ("gitconfig-mode.el")))
(package! gitignore-mode
	  	  :recipe (:host github :repo "magit/git-modes"
				 			 :files ("gitignore-mode.el")))
