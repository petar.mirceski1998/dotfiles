{ config, pkgs, ... }:

let
  customize = import ./customize.nix;
  fish-config = import ./configs/fish.nix;
  emacs-overlay = import ./overlays/emacs-overlay.nix;

  # Some local paths
  # this is the env path for the fish shell also
  homedir = builtins.getEnv "HOME";
  paths = [
    "${homedir}/.emacs.d/bin"
    "${homedir}/go/bin"
    "${homedir}/.local/bin"
    "${homedir}/miniconda3/bin"
    "${homedir}/Android/Sdk"
    "${homedir}/Android/Sdk/emulator"
    "${homedir}/Android/Sdk/tools"
    "${homedir}/Android/Sdk/tools/bin"
    "${homedir}/Android/Sdk/tools/platform-tools"
  ];

  my-st = (pkgs.st.override {
    patches =[./configs/st/st-dracula-0.8.2.diff];
  });

  my-st-desktop = (pkgs.makeDesktopItem {
    name = "st-256color";
    desktopName = "Simple Terminal";
    genericName = "Terminal";
    comment = "Suckless terminal emulator for X";
    icon = "utilities-terminal";
    type = "Application";
    # Geometry to force full screen
    exec = ''
      ${my-st}/bin/st -t "Simple Terminal" -g 240x240 -f "${customize.termFont}:antialias=true:autohint=true" tmux -u new-session -A -s main'';
    categories = "System;TerminalEmulator";
    extraEntries = ''
      Keywords=shell;prompt;command;commandline;cmd;
      StartupWMClass=st
    '';
  });


in {
  home = {
    # This value determines the Home Manager release that your
    # configuration is compatible with. This helps avoid breakage
    # when a new Home Manager release introduces backwards
    # incompatible changes.
    #
    # You can update Home Manager without changing this value. See
    # the Home Manager release notes for a list of state version
    # changes in each release.
    stateVersion = "20.03";

    # Environment variables
    sessionVariables = {
      VISUAL = "vim";
      EDITOR = "vim";
      PIP_REQUIRE_VIRTUALENV = 1; # Make global changes harder
      FZF_DEFAULT_OPTS = "--height 50% --reverse --ansi";

      PATH = builtins.concatStringsSep ":" (paths ++ ["$PATH"]);
    };

    # Dotfiles
    file = {
      ".doom.d" = {
        source = ../dots/doom.d;
        onChange = ".emacs.d/bin/doom -y sync";
      };
      ".config" = {
        source = ../dots/config;
        recursive = true;
      };
      # TODO Use builtins readDir and merge
      ".editorconfig".source = ../dots/editorconfig;
      ".tmux.conf".source = ../dots/tmux.conf;
      ".tmux.conf.local".source = ../dots/tmux.conf.local;
      ".condarc".source = ../dots/condarc;
      ".inputrc".source = ../dots/inputrc;
      ".npmrc".source = ../dots/npmrc;
      ".direnvrc".source = ../dots/direnvrc;
      ".prettierrc.toml".source = ../dots/prettierrc.toml;
    };

    packages = with pkgs; [
      # GUI apps
      my-st
      my-st-desktop

      # Fonts
      iosevka
      noto-fonts
      emacs-all-the-icons-fonts
      noto-fonts-emoji
      jetbrains-mono

      # Emacs Development
      coreutils
      ripgrep # (ripgrep.override {withPCRE2 = true;})
      fd # faster projectile indexing
      editorconfig-core-c # per-project style config
      gnutls # for TLS connectivity
      imagemagick # for image-dired
      zstd # for undo-tree compression

      # :checkers spell
      # :checkers grammar
      languagetool

      # :lang sh
      shellcheck
      shfmt

      # Development utils
      gnumake
      gitlab-runner
      azure-storage-azcopy
      sshfs

      # CLI Utils
      unzip
      zip
      unrar
      bat
      xclip
      dua
      wget
      curl
      exa # lsd
      broot # tree
      tokei
    ];
  };

  fonts.fontconfig.enable = true;

  nixpkgs.overlays = [
    emacs-overlay
  ];

  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    emacs = {
      enable = true;
      package = pkgs.emacsGit;
    };

    zathura.enable = true;
    feh.enable = true;

    tmux.enable = true;
    bat.enable = true;
    htop.enable = true;
    # starship.enable = true;
    fzf.enable = true;
    # Can enable when updated
    # broot.enable = true;
    jq.enable = true;

    direnv = {
      enable = true;
      enableFishIntegration = true;
    };

    git = {
      enable = true;
      # lfs.enable = true;
      userName = customize.userName;
      userEmail = customize.userEmail;
      extraConfig = { core.editor = "vim"; };
    };

    fish = fish-config;
  };

  news.display = "silent";
}
