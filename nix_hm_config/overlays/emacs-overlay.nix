let
  rev = "70fc82c8a6e56b689dda37117789193cc2f38665";
  sha256 = "1jz3bc2vhcgdv0fncih39d80r0qjr466hn7hxkmqjssvlsa390ib";
  emacs-overlay = import (builtins.fetchTarball {
    url = "https://github.com/nix-community/emacs-overlay/archive/${rev}.tar.gz";
    inherit sha256;
  });
in
  emacs-overlay
