{
enable = true;
shellAliases = {
  zf = ''z --pipe="fzf"'';
  initconda =
    ''eval ~/miniconda3/bin/conda "shell.fish" "hook" $argv | source'';
  pydev = "conda activate dev";
  ojcpp = "cppwarn main.cpp && oj test";
  ojpy = ''oj test -c "python main.py"'';
  top = "htop";
  du = "dua";
  df = "pydf";
  cat = "bat";
  free = "free -mt";
  psg = "ps aux | grep -v grep | grep -i -e VSZ -e";
  sc = "systemctl";
  ssc = "sudo systemctl";
  ls = "exa --group-directories-first";
  l = "ls -1";
  ll = "ls -lg";
  la = "ls -la";
  lt = "ls --tree";
  tree = "ls --tree";
  ps = "ps auxf";
  mkdir = "mkdir -pv";
  wget = "wget -c";
  myip = "curl http://ipecho.net/plain; echo";
  webify = "mogrify -resize 690> *.png";
};
interactiveShellInit = ''
  set acceptable_terms xterm-256color screen-256color xterm-termite
  if contains $TERM $acceptable_terms
      fish_vi_key_bindings
  end
  set fish_greeting
'';
}
