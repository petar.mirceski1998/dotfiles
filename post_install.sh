#!/usr/bin/env bash
set -e

# Go has some readonly permissions
# So run sudo rm or go clean -modcache
sudo rm -rf "$HOME/go"
sudo rm -rf "/usr/bin/yarn"
sudo rm -rf "usr/bin/node"

rm -rf "$HOME/.local/bin"
rm -rf "$HOME/.local/lib"
rm -rf "$HOME/.local/pipx"
rm -rf "$HOME/miniconda3"
rm -rf "$HOME/.cache/pypoetry"
rm -rf "$HOME/.config/yarn/global"
rm -rf "$HOME/.yarn"

bash ./setup/setup_conda.sh
bash ./setup/setup_python_utils.sh
bash ./setup/setup_node_utils.sh
bash ./setup/setup_vscode_extensions.sh

setxkbmap -option caps:escape
