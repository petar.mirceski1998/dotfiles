set nocompatible
set guioptions=Ace
filetype off

call plug#begin('~/.vim/plugged')

" Theme
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'joshdick/onedark.vim'
Plug 'liuchengxu/eleline.vim'
let g:eleline_powerline_fonts = 1

" Vim functionality fixes/enhancements
Plug 'sgur/vim-editorconfig'
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']
Plug 'tpope/vim-repeat'      " better repeat compatibility ('.')
Plug 'tpope/vim-fugitive'    " git in vim
Plug 'tpope/vim-eunuch'      " Use unix commands
Plug 'romainl/vim-cool'      " Fix search highlight
Plug 'airblade/vim-gitgutter'     " visual git diffs in the gutter
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
Plug 'justinmk/vim-gtfo'

" Projects
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' } " file explorer sidebar
Plug 'airblade/vim-rooter'  " fix cwd
let g:rooter_manual_only = 1
" Can use CocList and coc-lists instead
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }  " File searching
Plug 'junegunn/fzf.vim'

Plug 'bronson/vim-trailing-whitespace', { 'on': 'FixWhitespace' }
Plug 'godlygeek/tabular', { 'on': 'Tabularize' } " aligning text
Plug 'tpope/vim-commentary'
Plug 'justinmk/vim-sneak'         " 2-character search motions
Plug 'justinmk/vim-dirvish'
" Plug 'easymotion/vim-easymotion'

" Autoformat
Plug 'google/vim-maktaba'
Plug 'google/vim-codefmt'
" can use neoformat

" Langs
" TODO Switch syntax to tree sitter when available
Plug 'sheerun/vim-polyglot'
let g:polyglot_disabled = ['python', 'c', 'cpp']
Plug 'honza/vim-snippets'
Plug 'jackguo380/vim-lsp-cxx-highlight', { 'for': ['c', 'cpp'] }
Plug 'Vimjas/vim-python-pep8-indent', { 'for': 'python' }
Plug 'neoclide/jsonc.vim', { 'for': 'jsonc' }
" This requires python host
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins', 'for': 'python' }

Plug 'neoclide/coc.nvim', {'branch': 'release'}
" TODO Maybe install extensions via plug
let g:coc_global_extensions = [
      \'coc-pairs',
      \'coc-highlight',
      \'coc-tag',
      \'coc-snippets',
      \'coc-yank',
      \'coc-syntax',
      \'coc-git',
      \'coc-emoji',
      \'coc-calc',
      \'coc-dictionary',
      \'coc-marketplace',
      \'coc-eslint',
      \'coc-prettier',
      \'coc-jest',
      \'coc-webpack',
      \'coc-yaml',
      \'coc-json',
      \'coc-html',
      \'coc-go',
      \'coc-clangd',
      \'coc-vimlsp',
      \'coc-markdownlint',
      \'coc-pyright'
      \]
call plug#end()

" basic {{
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

" Theme Font and Appearance
colorscheme onedark
set guifont=Courier\ New\ 10
set belloff=all
set guioptions -=T
set guioptions -=m

" Performance
set lazyredraw
let g:loaded_node_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_python_provider = 0
" let g:loaded_python3_provider = 0

" Line numbers
set number relativenumber
set nu rnu

" Statusline
set laststatus=2

" Text formatting
set expandtab
set tabstop=4
set shiftwidth=4

" Search
set incsearch
set ignorecase
set smartcase

" Matching pairs
set showmatch
set matchtime=2
set matchpairs+=<:>

" More natural splits
set splitbelow
set splitright

" Completions
set completeopt=menu

" Clipboard
if has('unnamedplus')
    set clipboard=unnamedplus
else
    set clipboard=unnamed
endif


set diffopt+=internal,algorithm:patience
set pumheight=20

" coc.nvim {{ "
set hidden

set nobackup
set nowritebackup
setlocal noswapfile

set updatetime=100
set timeoutlen=500

if has("patch-8.1.1564")
  set signcolumn=number
else
  set signcolumn=yes
endif
set cmdheight=1
set shortmess+=c

" Makes gq work with lsp formatter
set formatexpr=CocAction('formatSelected')
" TODO Needs a newer neovim to enable
" set tagfunc=CocTagFunc


" Leader keys (mirrors Doom Emacs)
let mapleader = ' '
let maplocalleader = ' m'
nnoremap <silent> <leader> :WhichKey '<leader>'<CR>
nnoremap <silent> <leader><leader> :Files<CR>
nnoremap <silent> <leader>. :Dirvish %<CR>
nnoremap <silent> <leader>, :Buffers<CR>
" Leader + j = AnyJump
nnoremap <silent> <leader>bb :Buffers<CR>
nnoremap <silent> <leader>bl :BLines<CR>
nnoremap <silent> <leader>ff :Dirvish %<CR>
nnoremap <silent> <leader>fd :Delete<CR>
nnoremap <silent> <leader>fR :Rename
nnoremap <silent> <leader>fr :History <CR>
nnoremap <silent> <leader>pp :Files<CR>
" nnoremap <leader>si :CtrlPFunky<CR>
nnoremap <silent> <leader>sp :<C-u>Rg<CR>
nnoremap <silent> <leader>sl :<C-u>BLines<CR>
" Can also try vimagit
nnoremap <silent> <leader>gg :<C-u>Git<CR>
nnoremap <silent> <leader>gp :<C-u>Git push<CR>
nnoremap <silent> <leader>qq :wqa!<CR>
nnoremap <silent> <leader>qQ :qa!<CR>
nnoremap <silent> <leader>h :<C-u>h
" Turn off search highlighting
" noremap <silent> <leader>? :nohlsearch<CR>
" Formatting selected code.
xmap <leader>cf  <Plug>(coc-format-selected)
nmap <leader>cf  <Plug>(coc-format-selected)
" Symbol renaming.
nmap <leader>cr <Plug>(coc-rename)
nmap <leader>op :NERDTreeToggle<CR>
nmap <leader>o- :Ex<CR>
" Easymotion
nmap gs <Plug>(easymotion-prefix)
nmap <leader>sj <Plug>(SneakStreak)
" nmap <leader>sj <Plug>(SneakStreakBackward)

" Common Doom Bindings
nnoremap <leader>fs :w<CR>
nnoremap <leader>wv :vsplit<CR>
nnoremap <leader>ws :split<CR>
nnoremap <leader>wd :close<CR>
nnoremap <C-A>  ^
nnoremap <leader>wh <C-w>h
nnoremap <leader>wj <C-w>j
nnoremap <leader>wk <C-w>k
nnoremap <leader>wl <C-w>l

" Show all diagnostics.
nnoremap <silent><nowait> <leader>ce  :<C-u>CocList diagnostics<cr>
" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)
" Find symbol of current document.
nnoremap <silent><nowait> <leader>si  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <leader>cj  :<C-u>CocList -I symbols<cr>
nnoremap <silent><nowait> <leader>cm  :<C-u>CocList marketplace<cr>

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
function! s:show_documentation()
  if coc#util#has_float()
    call coc#util#float_hide()
  elseif (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
nnoremap <silent> K :call <SID>show_documentation()<CR>

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Next/prev buffer
nnoremap ]b :<C-u>bnext<CR>
nnoremap [b :<C-u>bprevious<CR>

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)


" Trigger to preserve indentation on pastes
set pastetoggle=<F12>
xnoremap p pgvy

" Mappings using CoCList:
" Manage extensions.
nnoremap <silent><nowait> <leader>le  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <leader>lc  :<C-u>CocList commands<cr>
" Resume latest coc list.
nnoremap <silent><nowait> <leader>lp  :<C-u>CocListResume<CR>
" Yank list
nnoremap <silent><nowait> <leader>ly  :<C-u>CocList -A --normal yank<CR>

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR :call CocAction('runCommand', 'editor.action.organizeImport')

augroup autoformat_settings
  autocmd FileType python AutoFormatBuffer black
  autocmd FileType sh AutoFormatBuffer shfmt
augroup END

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  " autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
  " Highlight the symbol and its references when holding the cursor.
  autocmd CursorHold * silent call CocActionAsync('highlight')
  " Organize imports on save
  " Causes some issues for .py files
  " autocmd BufWritePre *.go,*.ts,*.tsx,*.js,*.jsx :call CocAction('runCommand', 'editor.action.organizeImport')
  " Delete trailing white space on save
  autocmd BufWritePre * :FixWhitespace
augroup end

" TODO Some default mappings for Coc, should clean up

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of LS, ex: coc-tsserver
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
