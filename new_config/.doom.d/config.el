;;; Code:
(setq lsp-signature-auto-activate nil
      lsp-enable-indentation nil
      lsp-enable-on-type-formatting nil
      lsp-enable-symbol-highlighting nil
      ;; lsp-enable-file-watchers nil

      ;; Disable help mouse-overs for mode-line segments (i.e. :help-echo text).
      ;; They're generally unhelpful and only add confusing visual clutter.
      mode-line-default-help-echo nil
      show-help-function nil

      doom-scratch-buffer-major-mode 'org-mode
      treemacs-width 32
      display-line-numbers-width 4
      confirm-kill-emacs nil)

(set-frame-parameter nil 'internal-border-width 5)
;; Keybindings and remaps
(map!
 ;; See +lookup/definition to improve this
 :nv "go" #'xref-find-definitions-other-window
 :leader
 "C-SPC" #'projectile-find-file-other-window
 "C-." #'projectile-find-file-other-window
 )

(map!
 :desc "Change Input method"
 "C-\\" #'toggle-input-method
 )
;;
;;; Modules

;;; :completion ivy
;; (add-to-list 'ivy-re-builders-alist '(counsel-projectile-find-file . ivy--regex-plus))

;;; :ui doom-dashboard
;; (remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)


 ;;; :editor evil
(setq evil-split-window-below t
      evil-vsplit-window-right t)

;;; :tools direnv
(setq direnv-always-show-summary nil)

;;; :tools magit
(setq magit-repository-directories '(("~/repos" . 2))
      magit-save-repository-buffers nil
      ;; Don't restore the wconf after quitting magit
      magit-inhibit-save-previous-winconf t
      ;; Increase commit max length a bit
      git-commit-summary-max-length 72
      transient-values '((magit-rebase "--autosquash")
                         (magit-pull "--rebase")))

;;; Random stuff
(toggle-frame-maximized)
(setq projectile-project-search-path '("~/repos")
      which-key-idle-delay 0.1
      company-idle-delay 0
      company-dabbrev-downcase 0
      company-minimum-prefix-length 3
      )

;; :lang python
(add-hook! '(python-mode-hook c-mode-hook)
  (add-hook 'before-save-hook #'py-isort-before-save nil 'local))
(add-hook! 'python-mode-hook (setq-local flycheck-checker 'python-flake8))
(setq lsp-pyright-use-library-code-for-types t)

;; :lang js/ts
(add-hook! 'js2-mode-hook
  (add-hook 'before-save-hook #'format-all-buffer t))
(setq-hook! 'js2-mode-hook +format-with-lsp nil)

(setq-hook! 'typescript-mode-hook +format-with-lsp nil)
(setq-hook! 'typescript-tsx-mode-hook +format-with-lsp nil)

;; ;;; lang js/ts prettier mode
;; (setq +format-on-save-enabled-modes
;;       '(not emacs-lisp-mode  ; elisp's mechanisms are good enough
;;             sql-mode         ; sqlformat is currently broken
;;             tex-mode         ; latexindent is broken
;;             latex-mode
;;             rjsx-mode
;;             web-mode
;;             typescript-mode
;;             typescript-tsx-mode
;;             js2-jsx-mode
;;             js2-mode))
;;   (add-hook! '(web-mode-hook typescript-mode-hook typescript-tsx-mode-hook js2-mode-hook rjsx-mode-hook)
;;         (prettier-mode)))

(setq lsp-clients-typescript-log-verbosity "off")

;; lang latex
;; (setq pdf-latex-command "xelatex")


;; :lang Org mode
(add-hook 'org-mode-hook 'org-indent-mode)
(setq org-directory "~/repos/agenda"
      org-agenda-files '("~/repos/agenda")
      ;; org-default-notes-file (expand-file-name "notes.org" org-directory)
      org-ellipsis " ▼ "
      org-log-done 'time
      ;; org-journal-dir "~/Org/journal/"
      org-journal-date-format "%B %d, %Y (%A) "
      org-journal-file-format "%Y-%m-%d.org"
      org-hide-emphasis-markers t)

(setq org-superstar-headline-bullets-list '("⁖" "◉" "○" "✸" "✿"))

(setq org-latex-to-pdf-process
  '("xelatex -interaction nonstopmode %f"
     "xelatex -interaction nonstopmode %f")) ;; for multiple passes


;; THIS DOES NOT WORK FOR SOME REASONS INVESTIGATE
(add-hook! org-load
  (setq org-todo-keywords
        '((sequence "TODO" "|" "DONE" "FAILED(f)" "BLOCKED(b)"))))
(setq org-todo-keyword-faces
  '(("TODO" :foreground "#A9A1E1" :weight normal)
     ("DONE" :foreground "#50a14f" :weight normal :underline t)
     ("FAILED" :foreground "#FF6480" :weight normal :underline t)
     ("BLOCKED" :foreground "#FF6480" :weight normal :underline t)))

;; ui: Treesitter
 (use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(after! lsp-rust
  (setq lsp-rust-server 'rust-analyzer))

(load! "./customize.el")
