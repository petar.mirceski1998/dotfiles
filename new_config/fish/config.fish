direnv hook fish | source
pyenv init - | source

# neofetch --ascii ~/.config/neofetch/uwu.txt 1 2 3 4 5 6
alias neofetch='neofetch --ascii ~/.config/neofetch/uwu.txt 1 2 3 4 5 6'
alias wolframscript='wolfram'

export PATH="$HOME/.cargo/bin:$PATH"

fish_add_path "$HOME/.juliaup/bin/juliaup"

fish_add_path "/home/mircheskipetar/.juliaup/bin"

export PATH="$PATH:/opt/nvim/" 






