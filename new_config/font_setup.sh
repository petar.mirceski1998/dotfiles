#!/usr/bin/env bash

sudo wget https://download.jetbrains.com/fonts/JetBrainsMono-1.0.0.zip
unzip JetBrainsMono-1.0.0.zip
mv JetBrainsMono-*.ttf ~/.local/share/fonts/
