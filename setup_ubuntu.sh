#!/usr/bin/env bash
set -e

sudo apt update
sudo apt install -y curl

mkdir "$HOME/repos" -p  # Default emacs config needs this to exist
rm -rf "$HOME/.emacs.d" # Just in case
git clone https://github.com/hlissner/doom-emacs "$HOME/.emacs.d"

# Set up home Manager configuration
SCRIPT_PATH=$(dirname "$(realpath -s "$0")")
mkdir -p "$HOME/.config"
rm -rf "$HOME/.config/nixpkgs" # Just in case
ln -s "$SCRIPT_PATH/nix_hm_config" "$HOME/.config/nixpkgs"

bash "$SCRIPT_PATH/setup/setup_nix.sh"
# Set the caps button to escape
setxkbmap -option caps:swapescape
