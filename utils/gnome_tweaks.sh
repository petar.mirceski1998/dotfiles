#!/usr/bin/env bash
set -e

export GIO_EXTRA_MODULES=/usr/lib/x86_64-linux-gnu/gio/modules/
gsettings=/usr/bin/gsettings

# Caps as escape
$gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"
# Set up favorite apps
#FAVORITE_APPS_LIST="['firefox.desktop', 'emacs.desktop', 'st-256color.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Terminal.desktop', 'gnome-characters_gnome-characters.desktop']"
#$gsettings set org.gnome.shell favorite-apps "$FAVORITE_APPS_LIST"
# Disable audible bell, enable visual bell
#$gsettings set org.gnome.desktop.wm.preferences audible-bell false
#$gsettings set org.gnome.desktop.wm.preferences visual-bell true
# Can also use fullscreen-flash
#$gsettings set org.gnome.desktop.wm.preferences visual-bell-type frame-flash
# Reverse mouse scroll
#$gsettings set org.gnome.desktop.peripherals.mouse natural-scroll true
# Disable key repeat
#$gsettings set org.gnome.desktop.peripherals.keyboard repeat false
# Setup font scale. # TODO Look into fractional scaling
#$gsettings set org.gnome.desktop.interface text-scaling-factor 1.1
# Click on touchpad with two fingers
#$gsettings set org.gnome.desktop.peripherals.touchpad click-method fingers
# Enable night light always
#$gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
#$gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-automatic false
#$gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 20.0
#$gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 20.0
#$gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 4000
# TODO Speed up default keyboard repeat speed and reduce wait time
# TODO Disable search internet settings and set privacy settings
