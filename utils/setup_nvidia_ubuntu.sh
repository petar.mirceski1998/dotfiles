#!/usr/bin/env bash
set -e

sudo apt purge -y nvidia*
sudo add-apt-repository -y ppa:graphics-drivers
sudo apt update
sudo apt install ubuntu-drivers-common
sudo ubuntu-drivers autoinstall

# For nvidia gpus in docker
distribution=$(
	. /etc/os-release
	echo $ID$VERSION_ID
)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt update && sudo apt install -y nvidia-container-toolkit
sudo systemctl restart docker
