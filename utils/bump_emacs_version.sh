#!/usr/bin/env bash

set -e

EMACS_OVERLAY="https://github.com/nix-community/emacs-overlay"
SCRIPT_PATH=$(dirname "$(realpath -s "$0")")
HOME_NIX_DIR="$SCRIPT_PATH/../nix_hm_config/overlays/emacs-overlay.nix"

emacs_overlay() {
	rev=$(git ls-remote "$EMACS_OVERLAY" refs/heads/master | awk '{print $1}')
	hash=$(nix-prefetch-url --unpack "$EMACS_OVERLAY/archive/$rev.tar.gz")
	echo "Updating emacs-overlay.nix to $rev and $hash"
	sed -i "s|^  rev = .*|  rev = \"$rev\";|" "$HOME_NIX_DIR"
	sed -i "s|^  sha256 = .*|  sha256 = \"$hash\";|" "$HOME_NIX_DIR"
}

emacs_overlay
