#!/usr/bin/env bash
set -e

# Purge old versions
sudo apt-get purge -y docker docker-engine docker.io containerd runc

# Add repository
sudo apt update
sudo apt-get install \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg-agent \
	software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Use as non-root user
sudo usermod -aG docker "$USER"
