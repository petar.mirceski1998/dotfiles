#!/usr/bin/env bash
set -e

SCRIPT_PATH=$(dirname "$(realpath -s "$0")")
# shellcheck source=./init_conda.sh
. "$SCRIPT_PATH/init_conda.sh"
# shellcheck source=./declarative_utils.sh
. "$SCRIPT_PATH/declarative_utils.sh"

PIP_REQUIRE_VIRTUALENV=0 pip install --user -U pipx

SELECTED=$(clean_env "$SCRIPT_PATH/dependencies/pipx_environment.txt")
INSTALLED=$(pipx list | grep -oP "package\s\K\S*" | sort -u)

TO_DEL=$(get_diff <(echo "$SELECTED") <(echo "$INSTALLED"))
for_each "pipx uninstall" <(echo "$TO_DEL")

TO_INSTALL=$(get_diff <(echo "$INSTALLED") <(echo "$SELECTED"))
for_each "pipx install" <(echo "$TO_INSTALL")

pipx upgrade-all
