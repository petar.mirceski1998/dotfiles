#!/usr/bin/env bash
set -e

clean_env() {
	# Removes empty lines and comments from env file
	local ENV=$(sed '/^[[:blank:]]*#/d;s/#.*//;/^$/d' <"$1")
	# Trim whitespace and sort
	echo "$ENV" | awk '{$1=$1;print}' | sort -u
}

# Based on: https://unix.stackexchange.com/a/28159
get_diff() {
	grep -F -x -v -f "$@" || true
}

for_each() {
	local CMD="$1"
	local FILE
	FILE=$(cat "$2")
	if grep -q . <(echo "$FILE"); then
		while IFS= read -r line; do
			eval "$CMD" "$line"
		done <<<"$FILE"
	fi
}
