# CLI utils
awscli
cookiecutter
iredis
docker-compose
online-judge-tools
supervisor
proselint
grip
remarshal
pre-commit

# Python stuff
pipenv
bpython
black
isort
pyflakes
pydocstyle
