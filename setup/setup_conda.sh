#!/usr/bin/env bash
set -e

# Dependencies?
# sudo apt install -y wget bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion

CONDA_FILE="Miniconda3-latest-Linux-x86_64.sh"
wget "https://repo.anaconda.com/miniconda/$CONDA_FILE"
bash "$CONDA_FILE" -b
rm "$CONDA_FILE"

SCRIPT_PATH=$(dirname "$(realpath -s "$0")")
# shellcheck source=./init_conda.sh
. "${SCRIPT_PATH}/init_conda.sh"

conda install -y python=3.8
conda update -y -n base -c defaults conda
conda update -y -n base -c defaults --all

# ML development environment
# TODO Can change with one command when
# https://github.com/conda/conda/issues/9065
conda create -y --name dev
conda activate dev
conda config --env --set channel_priority strict
conda config --env --add channels conda-forge
conda config --env --add channels pytorch
conda env update --name dev --file "${SCRIPT_PATH}/dependencies/conda_environment.yml" --prune
conda update -y -n dev --all
