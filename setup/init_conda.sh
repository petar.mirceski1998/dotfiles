#!/usr/bin/env bash
set -e

. "$HOME/miniconda3/etc/profile.d/conda.sh"
conda activate base
