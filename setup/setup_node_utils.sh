#!/usr/bin/env bash
set -e

curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -
sudo apt-get install -y nodejs

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt install yarn

SCRIPT_PATH=$(dirname "$(realpath -s "$0")")
# shellcheck source=./declarative_utils.sh
. "$SCRIPT_PATH/declarative_utils.sh"

SELECTED=$(clean_env "$SCRIPT_PATH/dependencies/yarn_environment.txt")
INSTALLED=$(yarn global list | grep -oP 'info."\K.*(?=@)' | sort -u)

TO_DEL=$(get_diff <(echo "$SELECTED") <(echo "$INSTALLED"))
for_each "yarn global remove" <(echo "$TO_DEL")

TO_INSTALL=$(get_diff <(echo "$INSTALLED") <(echo "$SELECTED"))
for_each "yarn global add" <(echo "$TO_INSTALL")

yarn global upgrade
