#!/usr/bin/env bash

set -e

curl -L https://nixos.org/nix/install | sh

. "$HOME/.nix-profile/etc/profile.d/nix.sh"
echo "$HOME/.nix-profile/etc/profile.d/nix.sh" >> "$HOME/.bashrc"

# mkdir -m 0755 -p /nix/var/nix/{profiles,gcroots}/per-user/$USER
# NIX_PATH set because of https://github.com/NixOS/nix/issues/2033
# echo "export NIX_PATH=\$HOME/.nix-defexpr/channels\${NIX_PATH:+:}\$NIX_PATH"

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager

nix-channel --update
nix-shell '<home-manager>' -A install

# should be added automatically by installer to .profile
echo "if [ -e \"\$HOME\"/.nix-profile/etc/profile.d/nix.sh ]; then . \"\$HOME\"/.nix-profile/etc/profile.d/nix.sh; fi" >> "$HOME/.bashrc"

echo "export XDG_DATA_DIRS=\$HOME/.nix-profile/share:\$HOME/.share:\"\${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}\"" >>"$HOME/.profile"
echo "if [ -e \"\$HOME\"/.nix-profile/etc/profile.d/hm-session-vars.sh ]; then . \"\$HOME\"/.nix-profile/etc/profile.d/hm-session-vars.sh; fi" >>"$HOME/.bashrc"
